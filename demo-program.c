/*
 * Use with https://godbolt.org/
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define LOWER_BOUND (-1.0)
#define UPPER_BOUND (-LOWER_BOUND)

double RandomDouble(const double lower, const double upper)
{
    const double range = (upper - lower);
    const double div = RAND_MAX / range;

    return lower + (rand() / div);
}

double ApproximatePi(const unsigned iterations)
{
    double acc = 0.0;

    for (unsigned i = 0u; i < iterations; ++i)
    {
        const double x = RandomDouble(LOWER_BOUND, UPPER_BOUND);
        const double y = RandomDouble(LOWER_BOUND, UPPER_BOUND);

        acc += (x * x + y * y <= 1);
    }

    return 4.0 * acc / iterations;
}

int main(
    int argc,
    char *argv[])
{
    srand(time(NULL));
    const unsigned iterations = strtol(argv[1], NULL, 10u);
    const double pi = ApproximatePi(iterations);

    printf("pi = %lf\n", pi);

    return EXIT_SUCCESS;
}
