/*
 * https://www.onlinegdb.com/online_c_compiler
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int main()
{
    const int32_t raw_repr[] = {-4194304, 1104150527};
    
    printf("%lf\n", *((double *) raw_repr));

    return EXIT_SUCCESS;
}
